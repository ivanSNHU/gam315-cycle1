﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Death : MonoBehaviour
{
    //[SerializeField]
    //GameObject HighScore;

    [SerializeField]
    GameObject DeathCanvas;

    private float rounds;

    Text roundsSurvived;

    GameObject timer;

    public void Start()
    {
        roundsSurvived = GetComponent<Text>();

        timer = GameObject.FindGameObjectWithTag("Timer");
    }

    public void Restart()
    {
        DeathCanvas.SetActive(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }

    //public void HighBoard()
    //{
    //    HighScore.SetActive(true);
    //    DeathCanvas.SetActive(false);
    //}

    public void Update()
    {
        //rounds = timer.GetComponent<Timer>().round;
        rounds = timer.GetComponent<Timer>().round;

        roundsSurvived.text = "Score: "+ rounds;
    }
}
