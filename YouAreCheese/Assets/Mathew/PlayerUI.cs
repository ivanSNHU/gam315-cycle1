﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    Text playerText;

    [SerializeField]
    GameObject DeathCanvas;

    [SerializeField]
    GameObject deactive;

    [SerializeField]
    GameObject active;

    [SerializeField]
    GameObject trap;

    private static float health;
    private static float mouseTrap;
    GameObject mainPlayer;

    // Start is called before the first frame update
    void Start()
    {
        mainPlayer = GameObject.FindGameObjectWithTag("Player");

        playerText = GetComponent<Text>();
    }

    public void Update()
    {
        health = mainPlayer.GetComponent<CharacterController>().playerHealth;

        if (playerText != null)
        {
            playerText.text = "Health: " + health;
        }

        //checks players health
        if (health > 0)
        {
            Debug.Log("Player Health: " + health);
            DeathCanvas.SetActive(false);
            Time.timeScale = 1;
        }
        else if (health <= 0)
        {
            //if less than 0 activate death canvas
            DeathCanvas.SetActive(true);
            Time.timeScale = 0;
        }


        //if its the trap square

        //mouseTrap = CharacterController.playerMouseTraps;
        mouseTrap = mainPlayer.GetComponent<CharacterController>().playerMouseTraps;
        if (mouseTrap > 0)
        {
            active.SetActive(true);
            deactive.SetActive(false);
            trap.SetActive(true);
        }
        else
        {
            active.SetActive(false);
            deactive.SetActive(true);
            trap.SetActive(false);
        }

    }
}
