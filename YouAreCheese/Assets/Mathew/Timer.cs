﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    Text time;

    [SerializeField]
    float AddTime = 5;

    [SerializeField]
    float timelimit = 5;

    [SerializeField]
    public float round = 0;

    [SerializeField]
    bool timer;

    [SerializeField]
    bool rounds;

    public bool roundEnd = false;

    public float timeholder;


    public void Start()
    {
        time = GetComponent<Text>();
    }

    public void FixedUpdate()
    {        
        timeholder = timelimit;

        if(timer == true)
        {
            if (timelimit > 0)
            {
                timelimit -= Time.deltaTime;
                time.text = "Time: " + Mathf.Round(timelimit);                
            }
            else
            {
                rounds = true;
                roundEnd = true;
            }
        }

        /*while (roundEnd == true)
        {
            round += 1;
            Debug.Log("Rounds: " + round);
            timeholder += 5;
            timelimit = timeholder;
            roundEnd = false;
        }*/

         if(rounds == true)
        {
            time.text = "Rounds: " + round;
        }
    }

    public void RoundUpdate()
    {
        round += 1;
        if(AddTime <= 60)
        {
            AddTime += 20;
        }        
        timeholder += AddTime;
        timelimit = timeholder;
        rounds = false;
        roundEnd = false;
    }

}
