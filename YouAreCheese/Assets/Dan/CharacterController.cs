﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour
{
    [SerializeField] private Rigidbody2D playerRigidbody;
    [SerializeField] private GameObject playerObject;
    [SerializeField] GameObject mouseTrap;
    [SerializeField] private float playerSpeed = 10;
    [SerializeField] private float oldPlayerSpeed = 10;
    [SerializeField] private float dashSpeed = 20;
    [SerializeField] public int playerHealth = 3;
    [SerializeField] private int waxShield = 0;
    [SerializeField] public int playerMouseTraps = 0;
    [SerializeField] private bool isDashing = false;
    [SerializeField] private float dashDuration = 0.5f;
    [SerializeField] public float dashCooldown = 6.0f;
    [SerializeField] private float dashTimer = 0.0f;
    [SerializeField] private float horizontalAxis = 0;
    [SerializeField] private float verticalAxis = 0;
    [SerializeField] private bool isHurting = false;
    [SerializeField] private float hurtTimer = 0.0f;
    [SerializeField] private float hurtDuration = 0.2f;
    [SerializeField] private float knockbackAmount = 1.0f;



    void Start()
    {
        playerObject = GameObject.FindGameObjectWithTag("Player");
        playerRigidbody = playerObject.GetComponent<Rigidbody2D>();
    }

    // Fixed Update for velocity and movement calculations
    //Character controller sets velocity equal to the axis movement 
    void FixedUpdate()
    {
        //Activates the Dash mechanic, which gives a speed boost to the player
        //Replaces the playerSpeed with dashSpeed, then sets 
        if (isDashing)
        {
            playerSpeed = dashSpeed;
            isDashing = false;
        }


        //returns playerSpeed to normal after duration is over
        if (dashTimer <= (dashCooldown - dashDuration))
            playerSpeed = oldPlayerSpeed;


        if (isHurting && hurtTimer < 0)
            isHurting = false;

        //Timer management
        if (dashTimer > 0)
            dashTimer -= Time.deltaTime;
        if (hurtTimer > 0)
            hurtTimer -= Time.deltaTime;


        //Movement magic happens here
        if (!isHurting)
        {
            horizontalAxis = Input.GetAxis("Horizontal");
            verticalAxis = Input.GetAxis("Vertical");
            Vector2 temp = new Vector2(horizontalAxis, verticalAxis);
            temp = Vector2.ClampMagnitude(temp, 1);
            playerRigidbody.velocity = temp * playerSpeed;
        }

    }

    void Update()
    {
        //Activates Dash ability 
        //speed boost that lasts for dashDuration
        if (Input.GetKeyDown(KeyCode.LeftShift) && dashTimer <= 0)
        {
            isDashing = true;
            dashTimer = dashCooldown;
        }

        if (Input.GetButton("Fire1") && playerMouseTraps > 0)
        {
            playerMouseTraps--;
            GameObject newMouseTrap = Instantiate(mouseTrap);
            newMouseTrap.GetComponent<PowerUp>().powerupType = "trap";
            newMouseTrap.GetComponent<PowerUp>().trapSet = true;
            newMouseTrap.transform.position = transform.position;
            //Drop mousetrap code goes here
        }

        if (playerHealth <= 0)
        {
            Destroy(playerObject);
        }

        RotateToFaceVelocity();
    }

    private void RotateTest(float degrees)
    {
        transform.eulerAngles = new Vector3(
            transform.eulerAngles.x,
            transform.eulerAngles.y,
            Mathf.LerpAngle(transform.eulerAngles.z, degrees, Time.deltaTime * 5));
    }

    private void RotateToFaceVelocity()
    {
        Vector2 velocityHeading = playerRigidbody.velocity;
        if (velocityHeading.magnitude > 0.01f)
        {
            Vector2 targetHeading = new Vector2(
            transform.position.x + velocityHeading.x,
            transform.position.y + velocityHeading.y);
            RotateTest(Vector2.SignedAngle(Vector2.right, velocityHeading.normalized));
        }
    }


    public void TakeDamage(int damageVal, Vector3 enemyPosition)
    {
        //shield is 50, dmg is 75. 75-50 = 25. 25 goes to health
        //shield is 50, dmg is 5. 5-50 = -45
        if (!isHurting)
        {
            //int localDamageValue = damageVal;
            //if (waxShield >= localDamageValue)
            //{
            //    waxShield -= localDamageValue;
            //}
            //else
            //{
            //    localDamageValue -= waxShield;
            //    waxShield = 0;
            //    playerHealth -= localDamageValue;
            //}

            playerHealth -= damageVal;

            //Knockback
            isHurting = true;
            hurtTimer = hurtDuration;
            Vector2 knockbackDirection = (this.transform.position - enemyPosition).normalized;
            this.transform.Translate(knockbackDirection * knockbackAmount);

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("The cheese has entered a collision: " + collision.collider.tag);
        if (collision.collider.tag == "PowerUp")
        {
            if (collision.collider.GetComponent<PowerUp>().powerupType == "wax")
            {
                //add the bonus to me, yay!
                waxShield += 50;
                if (waxShield > 50)
                    waxShield = 50;
                collision.collider.GetComponent<PowerUp>().SetUsed();
            }
            else if (collision.collider.GetComponent<PowerUp>().powerupType == "trap" && !collision.collider.GetComponent<PowerUp>().trapSet)
            {
                //add the bonus to me, yay!
                playerMouseTraps = 1;
                collision.collider.GetComponent<PowerUp>().SetUsed();
            }      
            else if (collision.collider.GetComponent<PowerUp>().powerupType == "trap" && collision.collider.GetComponent<PowerUp>().trapSet)
            {
                Physics2D.IgnoreCollision(collision.gameObject.GetComponent<BoxCollider2D>(), gameObject.GetComponent<CircleCollider2D>());
            }
        }
    }
}
