﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doombaScript : MonoBehaviour
{
    Rigidbody2D rigBod;
    [SerializeField] public float forwardVelocity = 1000;
    [SerializeField] float intendedHeading;
    [SerializeField] bool rotating;
    [SerializeField] public int damage = 20;
    [SerializeField] float collisionStayTimer;
    [SerializeField] bool zig;
    [SerializeField] float passWidth;
    [SerializeField] float attackSpeed = 1;
    float lastAttack = 0;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("The DOOMBA lives!");
        rigBod = GetComponent<Rigidbody2D>();
        intendedHeading = rigBod.rotation;
        rotating = false;
        collisionStayTimer = 0;
        zig = false; ;
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        lastAttack += dt;
        //Vector2 velocity = rb.velocity;
        //rigBod.rotation = intendedHeading;
        if (rotating)
        {
            Rotate(dt);
        }
        else
        {
            MaintainSpeed(dt);
        }
    }

    void MaintainSpeed(float dt)
    {
        rigBod.velocity = transform.up * forwardVelocity;
    }

    void Rotate(float dt)
    {
        rigBod.velocity = Vector2.zero;
        rigBod.rotation = Mathf.LerpAngle(rigBod.rotation, intendedHeading, dt * 5);
        float a = rigBod.rotation;
        float b = intendedHeading;
        a %= 360;
        b %= 360;
        if (rigBod.rotation == intendedHeading)
        {
            rotating = false;
        }
        else if ((a - b) < 1 && (a - b) > -1)
        {
            rigBod.rotation = intendedHeading;
            rotating = false;
        }
        else if ((intendedHeading == 0) && ((a > 359 && a < 361) || (a < -359 && a > -361)))
        {
            rigBod.rotation = intendedHeading;
            rotating = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (CheckTag(collision))
        {
            BeginRotation();
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (CheckTag(collision))
        {
            collisionStayTimer += Time.fixedDeltaTime;
            if (collisionStayTimer > passWidth)
            {
                BeginRotation();
                collisionStayTimer = 0;
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        collisionStayTimer = 0;
        zig = !zig;
    }

    void BeginRotation()
    {
        if(zig)
        {
            intendedHeading += 90;
        }
        else
        {
            intendedHeading -= 90;
        }        
        intendedHeading %= 360;
        rotating = true;
    }

    bool CheckTag(Collision2D collision)
    {
        bool returnMe = false;

        string tag = collision.collider.tag;
        switch (tag)
        {
            case "Player":
                GameObject player = GameObject.FindGameObjectWithTag("Player");
                if (player.GetComponent<CharacterController>() != null && lastAttack > attackSpeed)
                {
                    player.GetComponent<CharacterController>().TakeDamage(damage, this.transform.position);
                    lastAttack = 0;
                }
                break;
            case "PowerUp":
                Destroy(collision.gameObject);
                break;
            case "Enemy":
                if (collision.gameObject.GetComponent<MouseAttackAndDamage>() != null)
                {
                    collision.gameObject.GetComponent<MouseAttackAndDamage>().TakeDamage(damage);
                }
                //Destroy(collision.gameObject);
                break;
            default:
                returnMe = true;
                break;
        }

        return returnMe;
    }
}
