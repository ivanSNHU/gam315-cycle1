﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public string powerupType;
    //public Sprite waxPowerup;
    public Sprite trapPowerup;
    public bool trapSet = false;
    public float rotateSpeed = 1;
    SpriteRenderer sr;

    // Start is called before the first frame update
    void Start()
    {
        sr = gameObject.GetComponent<SpriteRenderer>();
        //if(powerupType == "wax" && waxPowerup != null)
        //{
        //    sr.sprite = waxPowerup;
        //}
        //else 
        if (powerupType == "trap" && trapPowerup != null)
        {
            sr.sprite = trapPowerup;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        if(!trapSet)
        {
            transform.Rotate(Vector3.forward * dt * rotateSpeed);
        }
    }

    public void SetUsed()
    {
        Destroy(gameObject);
    }
}
