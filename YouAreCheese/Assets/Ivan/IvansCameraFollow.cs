﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IvansCameraFollow : MonoBehaviour
{
    public Transform target;
    public float strength;
    private float z;

    // Start is called before the first frame update
    void Start()
    {
        z = transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        Vector3 newPos = Vector3.Lerp(transform.position, target.position, dt * strength);
        newPos.z = z;
        transform.position = newPos;
    }
}
