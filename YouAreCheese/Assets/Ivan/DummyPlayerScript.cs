﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyPlayerScript : MonoBehaviour
{
    public float linearSpeed = 1;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        float jump = linearSpeed * dt;
        //Vector3 temp = transform.position;
        //if (Input.GetKey(KeyCode.A))
        //{
        //    gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.left * jump);
        //    //temp.x -= jump;
        //}
        //else if (Input.GetKey(KeyCode.D))
        //{
        //    gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.right * jump);
        //    //temp.x += jump;
        //}
        //if (Input.GetKey(KeyCode.W))
        //{
        //    gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * jump);
        //    //temp.y += jump;
        //}
        //else if (Input.GetKey(KeyCode.S))
        //{
        //    gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.down * jump);
        //    //temp.y -= jump;
        //}

        float a, b;
        a = Input.GetAxis("Horizontal");
        b = Input.GetAxis("Vertical");
        Vector2 ab = new Vector2(a, b);
        ab = Vector2.ClampMagnitude(ab, 1);
                
        gameObject.GetComponent<Rigidbody2D>().AddForce(ab * jump);

        //transform.position = temp;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("The cheese has entered a collision: " + collision.collider.tag);
        if(collision.collider.tag == "PowerUp")
        {
            //if(collision.collider.GetComponent<PowerUp>().powerupType == "wax")
            //{
            //    //add the bonus to me, yay!
            //}
            //else 
            if (collision.collider.GetComponent<PowerUp>().powerupType == "trap")
            {
                //add the bonus to me, yay!
            }
            collision.collider.GetComponent<PowerUp>().SetUsed();
        }
    }
}
