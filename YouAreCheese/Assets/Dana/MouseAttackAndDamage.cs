﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseAttackAndDamage : MonoBehaviour
{
    [SerializeField] float health = 100;
    [SerializeField] float attackSpeed;

    GameObject player;
    Coroutine waitToAttackC;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }    

    public void Attack(int damage = 1)
    {
        if(waitToAttackC == null)
        {
            if(player.GetComponent<CharacterController>() != null)
            {
                player.GetComponent<CharacterController>().TakeDamage(damage, this.transform.position);
                //GetComponent<mouseMovement>().ReturnToHole();
                GetComponent<mouseMovement>().gotCheese = true;
               // waitToAttackC = StartCoroutine(WaitToAttack());
            }
            else
            {
                Debug.Log("Character Controller script removed from player :(. Player's health is 0.");
            }            
        }       
    }

    private void Update()
    {
        if(health <= 0)
            GameObject.Find("MouseManager").GetComponent<MouseManager>().KillMouse(gameObject);
    }

    public void TakeDamage(float damage)
    {
        if(damage >= health || health <= 0)
        {
            GameObject.Find("MouseManager").GetComponent<MouseManager>().KillMouse(gameObject);
            //GetComponent<MouseManager>().KillMouse(gameObject);
        }
        else
        {
            health -= damage;
        }        
    }

    /*IEnumerator WaitToAttack()
    {
        yield return new WaitForSeconds(attackSpeed);
        waitToAttackC = null;
    }*/

}
