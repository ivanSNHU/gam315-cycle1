﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseManager : MonoBehaviour
{
    [SerializeField] GameObject mousePrefab;
    [SerializeField] GameObject PowerUpObject;

    [SerializeField] float spawnTime = 5;
    [SerializeField] float speedMultiplier = 1;
    [SerializeField] int numberOfMiceInScene = 0;
    [SerializeField] int micePerRound = 5;
    [SerializeField] int miceAllowedInScene = 3;

    [SerializeField] List<GameObject> spawnpoints;
    [SerializeField] List<GameObject> mice = new List<GameObject>(); //updated list of mice gameobjects in scene 

    public Timer timerScript;    
    
    Coroutine waitToSpawnC;    

    void Update()
    {
        //spawns mouse every x seconds via coroutine
        //allows x amount of mice per round; update once levels in place
        if (waitToSpawnC == null && numberOfMiceInScene < miceAllowedInScene && !timerScript.roundEnd) 
        {
            int spawnPoint = RandomSpawnpoint();
            numberOfMiceInScene++;
            
            GameObject mouse = Instantiate(mousePrefab, spawnpoints[spawnPoint].transform.position, spawnpoints[spawnPoint].transform.rotation);
            mice.Add(mouse);
            mouse.GetComponent<mouseMovement>().mouseManagerScript = this;
            mouse.GetComponent<mouseMovement>().mySpawnPoint = spawnpoints[spawnPoint].transform;
            mouse.GetComponent<mouseMovement>().speed *= speedMultiplier;

            waitToSpawnC = StartCoroutine(WaitToSpawn());
        }  

        if(timerScript.roundEnd && GameObject.FindGameObjectWithTag("Enemy") == null)
        {
            mice.Clear();
            numberOfMiceInScene = 0;
            timerScript.RoundUpdate();
            IncreaseDifficulty();
        }
       
    }

    void IncreaseDifficulty() //update with better scaling
    {
        if (speedMultiplier <= 5)
        {
            speedMultiplier += 0.5f;
        }
        if (spawnTime >= 2)
        {
            spawnTime -= 0.5f;
        }
        if (micePerRound <= 30)
        {
            micePerRound += 5;
        }
        if (miceAllowedInScene <= 20)
        {
            miceAllowedInScene += 2;
        }
    }

    int RandomSpawnpoint()
    {
        int index = Random.Range(0, 4);
        return index;
    }

    public void KillMouse(GameObject mouse)
    {
        if (!mouse.GetComponent<mouseMovement>().gotCheese && !timerScript.roundEnd)
        {
            if (Random.value < .2f)
            {
                GameObject newPowerUp = Instantiate(PowerUpObject);
                newPowerUp.transform.position = mouse.transform.position;
                if (Random.value <= .5)
                {
                    newPowerUp.GetComponent<PowerUp>().powerupType = "wax";
                }
                else
                {
                    newPowerUp.GetComponent<PowerUp>().powerupType = "trap";
                }
            }
        }

        mice.Remove(mouse);
        Destroy(mouse);        
    }

    IEnumerator WaitToSpawn()
    {
        yield return new WaitForSeconds(spawnTime);
        waitToSpawnC = null;
    }    
}
