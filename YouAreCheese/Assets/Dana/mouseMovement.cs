﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class mouseMovement : MonoBehaviour
{
    public float speed;
    [SerializeField] float distanceFromPlayer;
    [SerializeField] bool stuckInTrap = false;
    [SerializeField] float trappedTime = 5f;
    Rigidbody2D rb2d;

    GameObject player;
    MouseAttackAndDamage mouseAandD;
    public MouseManager mouseManagerScript;
    public Transform mySpawnPoint;
    Coroutine trappedC;

    public bool chase = false;
    public bool walkingTowardPoint = false;
    public bool gotCheese = false;
    public Vector3 targetRandom;
    public Collider2D[] hitColliders;

    [SerializeField] Vector2 actualVelocity;
    [SerializeField] Vector2 normalizedVelocity;
    [SerializeField] Vector2 absoluteVelocity;
    [SerializeField] Vector2 zeroDegreesVector;
    [SerializeField] float calculatedAngle;

    Vector3 currentPos;
    Vector3 lastPos = Vector3.zero;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        mouseAandD = GetComponent<MouseAttackAndDamage>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (player != null)
        {
            if (!gotCheese)
            {
                if (Vector2.Distance(player.transform.position, transform.position) > distanceFromPlayer && !stuckInTrap && !mouseManagerScript.timerScript.roundEnd) //if mouse can move
                {
                    CheckIfPlayerIsNear();

                    if (chase)
                    {
                        //move the mouse by physics, to move toward the player
                        Vector2 targetPosition = player.transform.position;
                        MyMoveToward(targetPosition);
                    }
                    else
                    {
                        currentPos = transform.position;

                        if (!walkingTowardPoint)
                        {
                            targetRandom = Random.insideUnitSphere * 8;
                            targetRandom = new Vector3(targetRandom.x, targetRandom.y, -5);
                            walkingTowardPoint = true;
                        }
                        /*Vector2 targetPosition = new Vector2(
                                mySpawnPoint.position.x,
                                mySpawnPoint.position.y);
                        MyMoveToward(targetPosition);*/
                        MyMoveToward(targetRandom);
                        //transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
                        if (Vector2.Distance(targetRandom, transform.position) <= 0.5f)
                        {
                            walkingTowardPoint = false;
                        }

                        if (rb2d.velocity == Vector2.zero && currentPos == lastPos)
                        {
                            Debug.Log("not moving");
                            walkingTowardPoint = false;
                        }
                        lastPos = currentPos;
                    }
                }
                else if (mouseManagerScript.timerScript.roundEnd) //end of round behavior
                {
                    ReturnToHole();
                }
                else if (Vector2.Distance(player.transform.position, transform.position) <= distanceFromPlayer && !stuckInTrap) //if in attack range, attack
                {
                    mouseAandD.Attack();
                }
            }
            else
            {
                ReturnToHole();
            }
        }
        RotateToFaceVelocity();
    }

    public void ReturnToHole()
    {
        StopCoroutine(Trapped());
        trappedC = null;
        chase = false;
        walkingTowardPoint = false;

        Vector3 target = new Vector3(mySpawnPoint.position.x, mySpawnPoint.position.y, transform.position.z);
        MyMoveToward(target);

        if (Vector2.Distance(mySpawnPoint.position, transform.position) < 0.5f)//if close to spawn point
        {
            mouseManagerScript.KillMouse(gameObject);
        }
    }

    private void MyMoveToward(Vector2 target)
    {
        Vector2 myPosition = transform.position;
        Vector2 heading = target - myPosition;
        heading.Normalize();

        rb2d.AddForce(heading * speed * Time.deltaTime);
    }

    private void RotateTest(float degrees)
    {
        transform.eulerAngles = new Vector3(
            transform.eulerAngles.x,
            transform.eulerAngles.y,
            Mathf.LerpAngle(transform.eulerAngles.z, degrees, Time.deltaTime * 5));
    }

    private void RotateToFaceVelocity()
    {
        Vector2 velocityHeading = rb2d.velocity;
        if (velocityHeading.magnitude > 0.01f)
        {
            Vector2 targetHeading = new Vector2(
            transform.position.x + velocityHeading.x,
            transform.position.y + velocityHeading.y);
            RotateTest(Vector2.SignedAngle(Vector2.right, velocityHeading.normalized));
        }
    }

    void CheckIfPlayerIsNear()
    {
        hitColliders = Physics2D.OverlapCircleAll(transform.position, 5.2f);

        for (int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].gameObject == player)
            {
                chase = true;
                walkingTowardPoint = false;
                i = hitColliders.Length;
            }
            else
            {
                chase = false;
            }
        }
    }

    //reacts to trap pickup; needs to be updated once there is an inventory
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "PowerUp" &&
            collision.collider.GetComponent<PowerUp>().powerupType == "trap" &&
            collision.collider.GetComponent<PowerUp>().trapSet &&
            trappedC == null)
        {
            stuckInTrap = true;
            trappedC = StartCoroutine(Trapped());
            collision.collider.GetComponent<PowerUp>().SetUsed();
        }
        else if (collision.collider.tag == "PowerUp")//|| collision.collider.tag == "Enemy")
        {
            Physics2D.IgnoreCollision(collision.gameObject.GetComponent<BoxCollider2D>(), gameObject.GetComponent<BoxCollider2D>());
        }
        else if(gotCheese && collision.collider.tag == "Player")
        {
            Physics2D.IgnoreCollision(collision.gameObject.GetComponent<CircleCollider2D>(), gameObject.GetComponent<BoxCollider2D>());
        }
    }

    IEnumerator Trapped()
    {
        yield return new WaitForSeconds(trappedTime);
        trappedC = null;
        stuckInTrap = false;
    }

}
