﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class oldmouseMovement : MonoBehaviour
{
    public float speed;
    [SerializeField] float distanceFromPlayer;
    [SerializeField] bool stuckInTrap = false;
    [SerializeField] float trappedTime = 5f;

    GameObject player;
    MouseAttackAndDamage mouseAandD;
    public MouseManager mouseManagerScript;
    public Transform mySpawnPoint;
    Coroutine trappedC;

    public bool chase = false;
    public bool walkingTowardPoint = false;
    public bool gotCheese = false;
    public Vector3 target;
    public Collider2D[] hitColliders;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        mouseAandD = GetComponent<MouseAttackAndDamage>();
    }

    void Update()
    {
        if (player != null)
        {
            if (!gotCheese)
            {
                if (Vector2.Distance(player.transform.position, transform.position) > distanceFromPlayer && !stuckInTrap && !mouseManagerScript.timerScript.roundEnd) //if mouse can move
                {
                    CheckIfPlayerIsNear();

                    if (chase)
                    {
                        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime); //move towards player 
                    }
                    else
                    {
                        if (!walkingTowardPoint)
                        {
                            target = Random.insideUnitSphere * 8;
                            target = new Vector3(target.x, target.y, -5);
                            walkingTowardPoint = true;
                        }

                        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);

                        if (Vector2.Distance(target, transform.position) <= 0)
                        {
                            walkingTowardPoint = false;
                        }
                    }
                }
                else if (mouseManagerScript.timerScript.roundEnd) //end of round behavior
                {
                    ReturnToHole();
                }
                else if (Vector2.Distance(player.transform.position, transform.position) <= distanceFromPlayer && !stuckInTrap) //if in attack range, attack
                {
                    mouseAandD.Attack();
                }
            }
            else
            {
                ReturnToHole();
            }
        }
    }

    public void ReturnToHole()
    {
        StopCoroutine(Trapped());
        trappedC = null;
        chase = false;
        walkingTowardPoint = false;

        Vector3 target = new Vector3(mySpawnPoint.position.x, mySpawnPoint.position.y, transform.position.z);
        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);

        if (Vector2.Distance(mySpawnPoint.position, transform.position) < 0.5f)//if close to spawn point
        {
            mouseManagerScript.KillMouse(gameObject);
        }
    }

    void CheckIfPlayerIsNear()
    {
        hitColliders = Physics2D.OverlapCircleAll(transform.position, 4f);

        for (int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].gameObject == player)
            {
                chase = true;
                walkingTowardPoint = false;
                i = hitColliders.Length;
            }
            else
            {
                chase = false;
            }
        }
    }

    //reacts to trap pickup; needs to be updated once there is an inventory
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "PowerUp" &&
            collision.collider.GetComponent<PowerUp>().powerupType == "trap" &&
            collision.collider.GetComponent<PowerUp>().trapSet &&
            trappedC == null)
        {
            Debug.Log("mouse stuck");
            stuckInTrap = true;
            trappedC = StartCoroutine(Trapped());
            collision.collider.GetComponent<PowerUp>().SetUsed();
        }
        else if (collision.collider.tag == "PowerUp")
        {
            Physics2D.IgnoreCollision(collision.gameObject.GetComponent<BoxCollider2D>(), gameObject.GetComponent<PolygonCollider2D>());
        }
    }

    IEnumerator Trapped()
    {
        yield return new WaitForSeconds(trappedTime);
        trappedC = null;
        stuckInTrap = false;
    }

}
